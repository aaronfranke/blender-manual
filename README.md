# Blender Documentation ([latest manual](https://docs.blender.org/manual/en/dev/))

Welcome to the Blender User Documentation project.
We are actively searching for contributors and documentation module owners.

The Blender Manual is written using `reStructuredText` (RST)
and is built with [Sphinx](http://www.sphinx-doc.org/en/stable/).

If you want to start contributing or want to have a look at the manual,
here we have some instructions:


## How to build & edit the docs locally

Before you start contributing it may be helpful to read the style guides
which give instructions on how to write with RST and some goals of the manual.

- [Markup Style Guide](https://docs.blender.org/manual/en/dev/contribute/guides/markup_guide.html)
- [Writing Style Guide](https://docs.blender.org/manual/en/dev/contribute/guides/writing_guide.html)

Now that you are familiar with the process, you can get setup,
edit, and contribute your changes by following these links:

- [Install](https://docs.blender.org/manual/en/dev/contribute/install/index.html)
- [Build](https://docs.blender.org/manual/en/dev/contribute/build.html)
- [Edit](https://docs.blender.org/manual/en/dev/contribute/editing.html)
- [Build Again](https://docs.blender.org/manual/en/dev/contribute/build.html)
- [Open Pull Request](https://docs.blender.org/manual/en/dev/contribute/pull_requests.html)

**Note:**
We realize this may be intimidating if you're not familiar with Git
or making patches, you may also submit the modified file as an
[Issue](https://projects.blender.org/blender/blender-manual/issues/new).


## Links

- **Manual Docs**: [Contributing Guide](https://docs.blender.org/manual/en/dev/contribute/index.html) (the process for editing the manual).
- **Source Files**: [Manual Repository](https://projects.blender.org/blender/blender-manual) (Git repository).
- **[Developer forum](https://devtalk.blender.org/c/documentation/12)**
- **Administrators**: @blendify @fsiddi


## Documentation Team

The coordination of the Blender Manual is run by a set of **Administrators**,
which are responsible for managing the whole project and infrastructures (listed above).

If you are interested in becoming a contributor, please contact us on the
[developer forum](https://devtalk.blender.org/c/documentation/12).


# Translations

For translations, we use Sphinx’s internationalization package.
To get started see the [contribution guide for translations](https://docs.blender.org/manual/en/dev/contribute/index.html#translations).


## Links

- **Source Files**: [Translation's Repository](https://projects.blender.org/blender/blender-manual-translations) (git repository).
- **Forum**: [Documentation category on Developer Talk](https://devtalk.blender.org/c/documentation)
- **Administrators**: @blendify , @fsiddi

**Note:*
If you are a new translator who wants to start translating a new language
that is not listed open an issue on the 
[issue tracker](https://projects.blender.org/blender/blender-manual/issues)
and we would be glad to set it up for you.
