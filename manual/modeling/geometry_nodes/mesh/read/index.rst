
###################
  Read Mesh Nodes
###################

.. toctree::
   :maxdepth: 1

   edge_angle.rst
   edge_neighbors.rst
   edge_vertices.rst
   edges_to_face_groups.rst
   face_area.rst
   face_group_boundaries.rst
   face_neighbors.rst
   face_is_planar.rst
   is_shade_smooth.rst
   mesh_island.rst
   shortest_edge_paths.rst
   vertex_neighbors.rst
